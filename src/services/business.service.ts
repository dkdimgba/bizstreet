import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';

@Injectable()
export class BusinessServices {
    // baseUrl:string = 'http://localhost:8000/api/';
    baseUrl:string = 'http://159.203.127.238:8000/api/';
    constructor(private _http:Http) { }

    getCategories(){
        let header = new Headers({
            'Content-Type':'application/x-www-form-urlencoded' , 
            'Authorization': 'Token 0a739470c2059fbf1698832774c0f0c00531e189'});
        let options = new RequestOptions({headers:header});
        return this._http.get(`${this.baseUrl}categories/`, options)
        .map(res => res.json())
    }

    // Get CIties
    getCities(){
        let header = new Headers({
            'Content-Type':'application/x-www-form-urlencoded' , 
            'Authorization': 'Token 0a739470c2059fbf1698832774c0f0c00531e189'});
        let options = new RequestOptions({headers:header});
        return this._http.get(`${this.baseUrl}cities/`, options)
        .map(res => res.json())
    }

    // Get Areas in a city
    getCityAreas(cId){
        let header = new Headers({
            'Content-Type':'application/x-www-form-urlencoded' , 
            'Authorization': 'Token 0a739470c2059fbf1698832774c0f0c00531e189'});
        let options = new RequestOptions({headers:header});
        return this._http.get(`${this.baseUrl}cityAreas/`+cId+ `/`, options)
        .map(res => res.json())
    }

    // Get Areas
    getAreas(){
        let header = new Headers({
           'Content-Type':'application/x-www-form-urlencoded' , 
           'Authorization': 'Token 0a739470c2059fbf1698832774c0f0c00531e189'});
        let options = new RequestOptions({headers:header});
        return this._http.get(`${this.baseUrl}areas/`, options)
        .map(res => res.json())
    }

    // Register Business
    addBusiness(business){
        let header = new Headers({
            'Content-Type': 'application/json' , 
            'Authorization': 'Token 0a739470c2059fbf1698832774c0f0c00531e189',
            'X-CSRFToken': '{{ csrf_token|escapejs }}'});
        let options = new RequestOptions({headers:header}); // ... Set content type to JSON

        return this._http.post(`${this.baseUrl}businessPost/`, business, options)
        .map(res => res.json());
    }

    // Update Business
    updateProfile(business){
        let header = new Headers({
            'Content-Type': 'application/json' , 
            'Authorization': 'Token 0a739470c2059fbf1698832774c0f0c00531e189',
            'X-CSRFToken': '{{ csrf_token|escapejs }}'});
        let options = new RequestOptions({headers:header}); // ... Set content type to JSON

        return this._http.patch(`${this.baseUrl}business/`+business.id+`/`, business, options)
        .map(res => res.json());
    }

    // Get Areas in a city
    getBusinessDetails(bId){
        let header = new Headers({
            'Content-Type':'application/x-www-form-urlencoded' , 
            'Authorization': 'Token 0a739470c2059fbf1698832774c0f0c00531e189'});
        let options = new RequestOptions({headers:header});
        return this._http.get(`${this.baseUrl}business/`+bId+ `/`, options)
        .map(res => res.json())
    }

    loginBiz(user){
        // let body = JSON.stringify(user)
        let header = new Headers({
            'Content-Type':'application/x-www-form-urlencoded' , 
            'Authorization': 'Token 0a739470c2059fbf1698832774c0f0c00531e189'});
        let options = new RequestOptions({headers:header});
        return this._http.get(`${this.baseUrl}loginBiz/`+user.biz_email, options)
        .map(res => res.json());
    }

    // Search Businesses Based on location and category
    getBusinessesSearch(location,category){
        let header = new Headers({
            'Content-Type':'application/x-www-form-urlencoded' , 
            'Authorization': 'Token 0a739470c2059fbf1698832774c0f0c00531e189'});
        let options = new RequestOptions({headers:header});
        return this._http.get(`${this.baseUrl}businessSearch/`+location+ `/`+category+ `/`, options)
        .map(res => res.json())
    }

    // Search Businesses Based on location and category
    getBusinessesSearchFilter(string){
        let header = new Headers({
            'Content-Type':'application /x-www-form-urlencoded' , 
            'Authorization': 'Token 0a739470c2059fbf1698832774c0f0c00531e189'});
        let options = new RequestOptions({headers:header});
        return this._http.get(`${this.baseUrl}search/?search=`+string, options)
        .map(res => res.json())
    }


    // Search Businesses Based on location and category
    getBusinessesDetails(bid){
        let header = new Headers({
            'Content-Type':'application/x-www-form-urlencoded' , 
            'Authorization': 'Token 0a739470c2059fbf1698832774c0f0c00531e189'});
        let options = new RequestOptions({headers:header});
        return this._http.get(`${this.baseUrl}businessDetails/`+bid+ `/`, options)
        .map(data => data.json())
    }

    // Send MSG
    sendMSG(MSG){
        let header = new Headers({
            'Content-Type': 'application/json' , 
            'Authorization': 'Token 0a739470c2059fbf1698832774c0f0c00531e189',
            'X-CSRFToken': '{{ csrf_token|escapejs }}'});
        let options = new RequestOptions({headers:header}); // ... Set content type to JSON

        return this._http.post(`${this.baseUrl}contacts/`, MSG, options)
        .map(res => res.json());
    }

    // Search Businesses Based on location and category
    getBusinessMessages(bid){
        let header = new Headers({
            'Content-Type':'application/x-www-form-urlencoded' , 
            'Authorization': 'Token 0a739470c2059fbf1698832774c0f0c00531e189'});
        let options = new RequestOptions({headers:header});
        return this._http.get(`${this.baseUrl}businessContacts/`+bid+`/`, options)
        .map(data => data.json())
    }

    // Get all Businesses
    getBusinesses() {
        let header = new Headers({
            'Content-Type': 'application/json' , 
            'Authorization': 'Token 0a739470c2059fbf1698832774c0f0c00531e189',
            'X-CSRFToken': '{{ csrf_token|escapejs }}'});
        let options = new RequestOptions({headers:header});
        return this._http.get(`${this.baseUrl}businesses/`, options)
        .map(data => data.json())
    }
}
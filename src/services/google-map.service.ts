import { Injectable } from '@angular/core';
import { Http } from '@angular/http';

@Injectable()
export class MapService {
  baseUrl;
  API_KEY:string = 'AIzaSyDC_NZTxW64JvFQB1vL9JQ--WxKx_0050w';

  
  constructor(private _http:Http) { }

  getCoordinates(address){
    this.baseUrl = 'https://maps.googleapis.com/maps/api/geocode/json?address='+address+'&key='+this.API_KEY;
    // let header = new Headers({
    						// 'Content-Type':'application/json'});
    // let options = new RequestOptions({headers:header});
    return this._http.get(`${this.baseUrl}`)
    .map(res => res.json())
  }

}

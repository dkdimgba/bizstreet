import { NgModule, ErrorHandler } from '@angular/core';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { Geolocation } from '@ionic-native/geolocation';

import { Ng2FilterPipeModule } from 'ng2-filter-pipe';

// components
import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { LoginPage } from '../pages/login';
import { SignupPage } from '../pages/signup';
import { DashboardPage } from '../pages/dashboard';
import { SearchPage } from '../pages/search';
import { SearchListPage } from '../pages/search-list';
import { SearchResultPage } from '../pages/search-result';
import { SearchMapPage } from '../pages/search-map';
import { BusinessDetailsPage } from '../pages/business-details';
import { ProfilePage } from '../pages/profile';
import { MessagesPage } from '../pages/messages';
import { HealthInsurancePage } from '../pages/health-insurance';
import { SmetipsPage } from '../pages/smetips';
import { CacRegisterPage } from '../pages/cac-register';
import { GalleryPage } from '../pages/gallery';
import { GetloanPage } from '../pages/getloan';
import { SpecialOffersPage } from '../pages/special-offers';
// Services
import { MapService } from '../services/google-map.service';
import { BusinessServices } from '../services/business.service';
import { TabsService } from '../services/tab.service';

// components
// import { TextAvatarDirective } from '../components/text-avatar';
 
@NgModule({
  declarations: [
    MyApp,
    HomePage,
    LoginPage,
    SignupPage,
    DashboardPage,
    SearchPage,
    SearchListPage,
    SearchResultPage,
    SearchMapPage,
    BusinessDetailsPage,
    ProfilePage,
    MessagesPage,
    HealthInsurancePage,
    SmetipsPage,
    CacRegisterPage,
    GalleryPage,
    GetloanPage,
    SpecialOffersPage
  ],
  imports: [
    IonicModule.forRoot(MyApp), 
    Ng2FilterPipeModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    LoginPage,
    SignupPage,
    DashboardPage,
    SearchPage,
    SearchListPage,
    SearchResultPage,
    SearchMapPage,
    BusinessDetailsPage,
    ProfilePage,
    MessagesPage,
    HealthInsurancePage,
    SmetipsPage,
    CacRegisterPage,
    GalleryPage,
    GetloanPage,
    SpecialOffersPage
  ],
  providers: [
    Geolocation,
    {provide: ErrorHandler, useClass: IonicErrorHandler}, MapService, BusinessServices, TabsService
    ]
})

export class AppModule {}
import { Component, OnInit } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

import { SearchResultPage } from '../search-result';
// import { SearchListPage } from '../search-list';

import { BusinessServices } from '../../services/business.service';

@Component({
  selector: 'page-search',
  templateUrl: 'search.html'
})
export class SearchPage implements OnInit {

	search:any = {};
	cities;
	categories;
	areas;
	AA:boolean = false;

  constructor(public navCtrl: NavController, public navParams: NavParams, private _busService:BusinessServices) {}

	ngOnInit() {
		this.getBusinessCategories();
		this.getCities();
	}

	getBusinessCategories(){	
		this._busService.getCategories()
		.subscribe(res =>{
			this.categories = res.results
			console.log(this.categories);
		})
	}

	getCities(){		
		this._busService.getCities()
		.subscribe(res =>{
			this.cities = res.results
			console.log(this.cities);	
		})
	}

	getCityAreas(cId){		
		this._busService.getCityAreas(cId)
		.subscribe(res =>{
			this.AA = true;
			this.areas = res.results
			console.log(this.areas);	
		})
	}

	ionViewDidLoad() {
		console.log('ionViewDidLoad SearchPage');
	}


	goToSearchList(string) {

	    this.navCtrl.push(SearchResultPage, {
	    	searchString: JSON.stringify(string)
	    });
	}

}

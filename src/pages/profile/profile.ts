import { Component, OnInit } from '@angular/core';
import { NavController, NavParams, ActionSheetController, ToastController, Platform, LoadingController, Loading } from 'ionic-angular';
import { Cookie } from 'ng2-cookies/ng2-cookies';
import { Camera, File, Transfer, FilePath } from 'ionic-native';

import { GalleryPage } from '../gallery';

import { MapService } from '../../services/google-map.service';
import { BusinessServices } from '../../services/business.service';

declare var cordova: any;

@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html'
})
export class ProfilePage implements OnInit {
	biz;
	bizData: any = {};
	categories;
	cities;
	areas;
	AA:boolean;
	Cordinates:any;

	lastImage: string = null;
  	loading: Loading;

	constructor(public navCtrl: NavController, public navParams: NavParams, public actionSheetCtrl: ActionSheetController, public toastCtrl: ToastController, public platform: Platform, public loadingCtrl: LoadingController, private _busService:BusinessServices, private _mapService:MapService) {}
 
	ngOnInit(){
		this.getBusinessDetails();
		this.getBusinessCategories();
		this.getCities();
		this.getAreas();
	}

	getBusinessCategories(){	
		this._busService.getCategories()
		.subscribe(res =>{
			this.categories = res.results
		})
	}

	getCities() {		
		this._busService.getCities()
		.subscribe(res =>{
			this.cities = res.results
			console.log(this.cities);	
		})
	}

	getAreas(){		
		this._busService.getAreas()
		.subscribe(res =>{
			this.AA = true;
			this.areas = res.results
			console.log(this.areas);	
		})
	}

	getCityAreas(cId){		
		this._busService.getCityAreas(cId)
		.subscribe(res =>{
			this.AA = true;
			this.areas = res.results
		})
	}

	getCoordinates(address){
		if(address) {
			let cleanAddress = address.replace(/ /g,'+');
			this._mapService.getCoordinates(cleanAddress)
			.subscribe(res => {

				this.Cordinates = res.results[0].geometry.location;
	            // this.Coord = true;
	            this.biz.biz_longitude = (Math.round(this.Cordinates.lng * Math.pow(10, 5)) / Math.pow(10, 5)).toFixed(5);
	            this.biz.biz_latitude = (Math.round(this.Cordinates.lat * Math.pow(10, 5)) / Math.pow(10, 5)).toFixed(5);
			})
		}
	}

	getBusinessDetails(){	
		let bizData = Cookie.get('userData');
		let bizDataParsed = JSON.parse(bizData);
		let bid = bizDataParsed.id;
		this._busService.getBusinessDetails(bid)
		.subscribe(res =>{
			this.biz = res;
		})
	}


	ionViewDidLoad() {
		console.log('ionViewDidLoad ProfilePage');
	}

	updateProfile(biz){
		
		if(biz.biz_location.id != null || biz.biz_location.id != undefined) { // Check if nested
			biz.biz_location = biz.biz_location.id; 
		}
		
		if(biz.biz_city.id != null || biz.biz_city.id != undefined) { // Check if nested
			biz.biz_city = biz.biz_city.id; 
		}
		
		if(biz.biz_category.id != null || biz.biz_category.id != undefined) { // Check if nested
			biz.biz_category = biz.biz_category.id; 
		}
		console.log(biz);
		this._busService.updateProfile(biz)
		.subscribe(res => {
			alert("Profile has been successfully Updated");
		}) 
	}

	// Action button for camera upload
	public presentActionSheet() {
		let actionSheet = this.actionSheetCtrl.create({
		title: 'Select Image Source',
		buttons: [
			{
			text: 'Load from Library',
			handler: () => {
				this.takePicture(Camera.PictureSourceType.PHOTOLIBRARY);
			}
			},
			{
			text: 'Use Camera',
			handler: () => {
				this.takePicture(Camera.PictureSourceType.CAMERA);
			}
			},
			{
			text: 'Cancel',
			role: 'cancel'
			}
		]
		});
		actionSheet.present();
	}

	// Method to take picture
	public takePicture(sourceType) {
		// Create options for the Camera Dialog
		var options = {
			quality: 100,
			sourceType: sourceType,
			saveToPhotoAlbum: false,
			correctOrientation: true
		};
		
		// Get the data of an image
		Camera.getPicture(options).then((imagePath) => {
			// Special handling for Android library
			if (this.platform.is('android') && sourceType === Camera.PictureSourceType.PHOTOLIBRARY) {
			FilePath.resolveNativePath(imagePath)
			.then(filePath => {
				let correctPath = filePath.substr(0, filePath.lastIndexOf('/') + 1);
				let currentName = imagePath.substring(imagePath.lastIndexOf('/') + 1, imagePath.lastIndexOf('?'));
				this.copyFileToLocalDir(correctPath, currentName, this.createFileName());
			});
			} else {
			var currentName = imagePath.substr(imagePath.lastIndexOf('/') + 1);
			var correctPath = imagePath.substr(0, imagePath.lastIndexOf('/') + 1);
			this.copyFileToLocalDir(correctPath, currentName, this.createFileName());
			}
		}, (err) => {
			this.presentToast('Error while selecting image.');
		});
	}

	// Create a new name for the image
	private createFileName() {
		var d = new Date(),
		n = d.getTime(),
		newFileName =  n + ".jpg";
		return newFileName;
	}
 
	// Copy the image to a local folder
	private copyFileToLocalDir(namePath, currentName, newFileName) {
		File.copyFile(namePath, currentName, cordova.file.dataDirectory, newFileName).then(success => {
			this.lastImage = newFileName;
		}, error => {
			this.presentToast('Error while storing file.');
		});
	}
 
	private presentToast(text) {
		let toast = this.toastCtrl.create({
			message: text,
			duration: 3000,
			position: 'top'
		});
		toast.present();
	}
	
	// Always get the accurate path to your apps folder
	public pathForImage(img) {
		if (img === null) {
			return '';
		} else {
			return cordova.file.dataDirectory + img;
		}
	}

	public uploadImage() {
		// Destination URL
		var url = "http://yoururl/upload.php";
		
		// File for Upload
		var targetPath = this.pathForImage(this.lastImage);
		
		// File name only
		var filename = this.lastImage;
		
		var options = {
			fileKey: "file",
			fileName: filename,
			chunkedMode: false,
			mimeType: "multipart/form-data",
			params : {'fileName': filename}
		};
 
		const fileTransfer = new Transfer();
		
		this.loading = this.loadingCtrl.create({
			content: 'Uploading...',
		});
		this.loading.present();
 
		// Use the FileTransfer to upload the image
		fileTransfer.upload(targetPath, url, options).then(data => {
			this.loading.dismissAll()
			this.presentToast('Image succesfully uploaded.');
		}, err => {
			this.loading.dismissAll()
			this.presentToast('Error while uploading file.');
		});
	}

    goToGallery() {
        this.navCtrl.push(GalleryPage);
    }
    
}



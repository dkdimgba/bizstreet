import { Component, OnInit } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

import { BusinessDetailsPage } from '../business-details';
import { SearchPage } from '../search';
import { HomePage } from '../home/home';

import { BusinessServices } from '../../services/business.service';

import { TabsService } from '../../services/tab.service';

@Component({
  selector: 'page-search-list',
  templateUrl: 'search-list.html'
})
export class SearchListPage implements OnInit {
	searchString;
	searchResults;
	areas;
	userFilter: any = { biz_name: '' };

  constructor(public navCtrl: NavController, public navParams: NavParams, private _busService:BusinessServices, public _tabs:TabsService) { }

  ngOnInit(){
  	this._tabs.show()

  	this.searchString = JSON.parse(this.navParams.get('searchString'));
  	console.log(this.searchString);

  
  	if (this.searchString.search_city) {
  		this.businessesSearch(this.searchString.search_city, this.searchString.search_category);
  		this.getCityAreas(this.searchString.search_city);
  	}else{
  		this.businessesSearchFilter(this.searchString);
  		// this.getCityAreas(this.searchString.search_city);
  	}
  }

	businessesSearch(loc,cat){
		this._busService.getBusinessesSearch(loc,cat)
		.subscribe(res => {
			this.searchResults = res.results;
			console.log(this.searchResults);
		})
	}

	businessesSearchFilter(str){
		this._busService.getBusinessesSearchFilter(str)
		.subscribe(res => {
			this.searchResults = res.results;
			console.log(this.searchResults);
		})
	}

	ionViewDidLoad() {
		console.log('ionViewDidLoad SearchListPage');
	}


	getCityAreas(cId){		
		this._busService.getCityAreas(cId)
		.subscribe(res =>{
			// this.AA = true;
			this.areas = res.results
			console.log(this.areas);	
		})
	}


	goToBusinessDetails(BID) {
	    this.navCtrl.push(BusinessDetailsPage, {
	    	bId : BID
	    });
	}

	goToSearch() {
	    this.navCtrl.push(SearchPage);
	}

	goToHome() {
	    this.navCtrl.push(HomePage);
	}

}

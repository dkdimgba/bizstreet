import { Component, OnInit } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

import { SearchListPage } from '../search-list';
import { SearchMapPage } from '../search-map';

@Component({
  selector: 'page-search-result',
  templateUrl: 'search-result.html'
})
export class SearchResultPage implements OnInit {

  searchString;
  tab1: any;
  tab2: any;
  params;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  	this.tab1 = SearchListPage;
  	this.tab2 = SearchMapPage;
    this.params = navParams.data;
  }

  ngOnInit(){
    this.searchString = this.navParams.get('searchString');
    console.log(this.searchString);
  }


  ionViewDidLoad() {
    console.log('ionViewDidLoad SearchResultPage');
  }

}

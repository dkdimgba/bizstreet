import { Component, OnInit } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

import { HomePage } from '../home/home';

@Component({
	selector: 'page-special-offers',
	templateUrl: 'special-offers.html'
})

export class SpecialOffersPage implements OnInit {
	searchString;
	searchResults;
	areas;
	userFilter: any = { biz_name: '' };

  	constructor(public navCtrl: NavController, public navParams: NavParams) { }

	ngOnInit(){
		
	}

	ionViewDidLoad() {
		console.log('ionViewDidLoad SearchListPage');
	}
	
	goToHome() {
	    this.navCtrl.push(HomePage);
	}

}

import { Component, OnInit } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
// import { Cookie } from 'ng2-cookies/ng2-cookies';

import { LoginPage } from '../login';
import { HomePage } from '../home/home';

import { MapService } from '../../services/google-map.service';
import { BusinessServices } from '../../services/business.service';

/*
  Generated class for the Signup page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/ 
@Component({
  selector: 'page-signup',
  templateUrl: 'signup.html'
})
export class SignupPage implements OnInit {
	biz:any = {};
	Cordinates:any;
	Coord:boolean;
	cities;
	categories;
	areas;
	AA:boolean = false;

	constructor(public navCtrl: NavController, public navParams: NavParams, private _mapService:MapService, private _busService:BusinessServices) {}

	ngOnInit() {
		this.getBusinessCategories();
		this.getCities();
	}

	goBackHome() {
        this.navCtrl.push(HomePage);
    }

	goToLogin() {
        this.navCtrl.push(LoginPage);
    }

	getBusinessCategories(){	
		this._busService.getCategories()
		.subscribe(res =>{
			this.categories = res.results
			console.log(this.categories);
		})
	}

	getCities(){		
		this._busService.getCities()
		.subscribe(res =>{
			this.cities = res.results
			console.log(this.cities);	
		})
	}

	getCityAreas(cId){		
		this._busService.getCityAreas(cId)
		.subscribe(res =>{
			this.AA = true;
			this.areas = res.results
			console.log(this.areas);	
		})
	}

	ionViewDidLoad() {
		console.log('ionViewDidLoad SignupPage');
	}

	signup(biz){
		console.log(biz);
		this._busService.addBusiness(biz)
		.subscribe(res =>{
			// Cookie.set('biz_id', res.id);
			// Cookie.set('biz_email', res.biz_email);
			// Cookie.set('biz_name', res.biz_name);
			// Cookie.set('biz_email', res.biz_email);
			this.navCtrl.push(LoginPage);
			console.log(res);
		})
	}

	getCoordinates(address){
		if(address) {
			let cleanAddress = address.replace(/ /g,'+');
			console.log(cleanAddress);
			this._mapService.getCoordinates(cleanAddress)
			.subscribe(res => {
				console.log(res);

				this.Cordinates = res.results[0].geometry.location;
	            console.log(this.Cordinates) ; 
	            this.Coord = true;
	            this.biz.biz_longitude = (Math.round(this.Cordinates.lng * Math.pow(10, 5)) / Math.pow(10, 5)).toFixed(5);
	            this.biz.biz_latitude = (Math.round(this.Cordinates.lat * Math.pow(10, 5)) / Math.pow(10, 5)).toFixed(5);
			})
		}
	}
}

import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

import { DashboardPage } from '../dashboard';

/*
  Generated class for the Getloan page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-getloan',
  templateUrl: 'getloan.html'
})
export class GetloanPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {}

  ionViewDidLoad() {
    console.log('ionViewDidLoad GetloanPage');
  }

  goToConfirm(){
		alert("Thank you for subscribing to our Get Loan Program for small businesses. One of our representative will get in touch with you using your preferd mode of communication.")
		this.navCtrl.push(DashboardPage);
	}

}

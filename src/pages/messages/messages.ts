import { Component, OnInit } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { Cookie } from 'ng2-cookies/ng2-cookies';

import { BusinessServices } from '../../services/business.service';

/*
  Generated class for the Messages page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-messages',
  templateUrl: 'messages.html'
})
export class MessagesPage implements OnInit {

	messages;

  constructor(public navCtrl: NavController, public navParams: NavParams, private _busService:BusinessServices) {}

  ngOnInit(){
  	this.getBusinessMessages();
  }

	getBusinessMessages(){	
    var bizData = Cookie.get('userData');
    var bizDataParsed = JSON.parse(bizData);
    let bId = bizDataParsed.id;
		// let bId = Cookie.get('biz_id');
		this._busService.getBusinessMessages(bId)
		.subscribe(res =>{
			this.messages = res.results;
			console.log(this.messages);
		})
	}


  ionViewDidLoad() {
    console.log('ionViewDidLoad MessagesPage');
  }

}

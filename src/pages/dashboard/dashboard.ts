import { Component, OnInit } from '@angular/core';
// import { MenuController } from 'ionic-angular';
import { NavController, MenuController, NavParams} from 'ionic-angular';
import { Cookie } from 'ng2-cookies/ng2-cookies';

import { ProfilePage } from '../profile';
import { MessagesPage } from '../messages';
import { HealthInsurancePage } from '../health-insurance';
import { SmetipsPage } from '../smetips';
import { CacRegisterPage } from '../cac-register';
import { GetloanPage } from '../getloan';

import { BusinessServices } from '../../services/business.service';

/*
  Generated class for the Dashboard page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
	selector: 'page-dashboard',
	templateUrl: 'dashboard.html'
	// styleUrls: ['./dashboard.scss']
})

export class DashboardPage implements OnInit {
	business:any;
	businessName: string;
	businessId: string;
	businessIntroMessage: string;
	businessLocation: string;
	bId;


	constructor(public navCtrl: NavController, menu: MenuController, public navParams: NavParams, private _busService:BusinessServices) {}

	ngOnInit(){
		this.getBusinessDetails();
	}

	getBusinessDetails(){	
		var bizData = Cookie.get('userData');
		var bizDataParsed = JSON.parse(bizData);
		let bId = bizDataParsed.id;
		this._busService.getBusinessDetails(bId)
		.subscribe(res =>{
			this.business = res;
			this.businessName = this.business.biz_name;
			this.businessId = bId;
			this.businessIntroMessage = this.business.biz_intro_message;
			this.businessLocation = this.business.biz_city;
			// this.businessCategory = this.business.biz_category;
		})
	}

	ionViewDidLoad() {
		console.log('ionViewDidLoad LoginPage');
	}

	goToEditProfile(){
		this.navCtrl.push(ProfilePage);
	}

	goToMessages(){
		this.navCtrl.push(MessagesPage);
	}

	goToHealthInsurance(){
		this.navCtrl.push(HealthInsurancePage);
	}

	goToSmetips(){
		this.navCtrl.push(SmetipsPage);
	}

	goToCacRegister(){
		this.navCtrl.push(CacRegisterPage);
	}

	goToGetloan(){
		this.navCtrl.push(GetloanPage);
	}
}

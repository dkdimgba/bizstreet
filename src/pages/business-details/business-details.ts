import { Component, OnInit } from '@angular/core';
import { NavController, NavParams, MenuController } from 'ionic-angular';

import { HomePage } from '../home/home';

import { BusinessServices } from '../../services/business.service';

/*
  Generated class for the BusinessDetails page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
	selector: 'page-business-details',
	templateUrl: 'business-details.html'
})

export class BusinessDetailsPage implements OnInit {
	BID;
	business:any;
	MS:boolean = false;
	MSG:any = {};

	constructor(public navCtrl: NavController, menu: MenuController, public navParams: NavParams, private _busService:BusinessServices) {}

	ngOnInit(){
		this.BID = this.navParams.get('bId');
		// console.log(this.BID);

		this._busService.getBusinessesDetails(this.BID)
		.subscribe(data => {
			this.business = data;
			console.log(data);
		})
	}


	ionViewDidLoad() {
		console.log('ionViewDidLoad BusinessDetailsPage');
	}

	sendMSG(MSG){
		MSG.bc_business = this.BID;
		this._busService.sendMSG(MSG)
		.subscribe(res => {
			alert("Message Sent");
			this.MS = false;
			this.MSG = {};
		})
	}


	goToHome() {
	    this.navCtrl.push(HomePage);
	}

}

import { Component } from '@angular/core';
import { NavController, NavParams, ActionSheetController, ToastController, Platform, LoadingController, Loading } from 'ionic-angular';

import { Camera, File, Transfer, FilePath } from 'ionic-native';

declare var cordova: any;
/*
  Generated class for the Gallery page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-gallery',
  templateUrl: 'gallery.html'
})

export class GalleryPage {

	lastImage: string = null;
  	loading: Loading;


	constructor(public navCtrl: NavController, public navParams: NavParams, public actionSheetCtrl: ActionSheetController, public toastCtrl: ToastController, public platform: Platform, public loadingCtrl: LoadingController) {}

	ionViewDidLoad() {
		console.log('ionViewDidLoad GalleryPage');
	}


	// Action button for camera upload
	public presentActionSheet() {
		let actionSheet = this.actionSheetCtrl.create({
		title: 'Select Image Source',
		buttons: [
			{
			text: 'Load from Library',
			handler: () => {
				this.takePicture(Camera.PictureSourceType.PHOTOLIBRARY);
			}
			},
			{
			text: 'Use Camera',
			handler: () => {
				this.takePicture(Camera.PictureSourceType.CAMERA);
			}
			},
			{
			text: 'Cancel',
			role: 'cancel'
			}
		]
		});
		actionSheet.present();
	}

	// Method to take picture
	public takePicture(sourceType) {
		// Create options for the Camera Dialog
		var options = {
			quality: 100,
			sourceType: sourceType,
			saveToPhotoAlbum: false,
			correctOrientation: true
		};
		
		// Get the data of an image
		Camera.getPicture(options).then((imagePath) => {
			// Special handling for Android library
			if (this.platform.is('android') && sourceType === Camera.PictureSourceType.PHOTOLIBRARY) {
			FilePath.resolveNativePath(imagePath)
			.then(filePath => {
				let correctPath = filePath.substr(0, filePath.lastIndexOf('/') + 1);
				let currentName = imagePath.substring(imagePath.lastIndexOf('/') + 1, imagePath.lastIndexOf('?'));
				this.copyFileToLocalDir(correctPath, currentName, this.createFileName());
			});
			} else {
			var currentName = imagePath.substr(imagePath.lastIndexOf('/') + 1);
			var correctPath = imagePath.substr(0, imagePath.lastIndexOf('/') + 1);
			this.copyFileToLocalDir(correctPath, currentName, this.createFileName());
			}
		}, (err) => {
			this.presentToast('Error while selecting image.');
		});
	}

	// Create a new name for the image
	private createFileName() {
		var d = new Date(),
		n = d.getTime(),
		newFileName =  n + ".jpg";
		return newFileName;
	}
 
	// Copy the image to a local folder
	private copyFileToLocalDir(namePath, currentName, newFileName) {
		File.copyFile(namePath, currentName, cordova.file.dataDirectory, newFileName).then(success => {
			this.lastImage = newFileName;
		}, error => {
			this.presentToast('Error while storing file.');
		});
	}
 
	private presentToast(text) {
		let toast = this.toastCtrl.create({
			message: text,
			duration: 3000,
			position: 'top'
		});
		toast.present();
	}
	
	// Always get the accurate path to your apps folder
	public pathForImage(img) {
		if (img === null) {
			return '';
		} else {
			return cordova.file.dataDirectory + img;
		}
	}

	public uploadImage() {
		// Destination URL
		var url = "http://projaro.com.ng/apps/bizstreet/img_api";
		
		// File for Upload
		var targetPath = this.pathForImage(this.lastImage);
		
		// File name only
		var filename = this.lastImage;
		
		var options = {
			fileKey: "file",
			fileName: filename,
			chunkedMode: false,
			mimeType: "multipart/form-data",
			params : {'fileName': filename}
		};
 
		const fileTransfer = new Transfer();
		
		this.loading = this.loadingCtrl.create({
			content: 'Uploading...',
		});
		this.loading.present();
 
		// Use the FileTransfer to upload the image
		fileTransfer.upload(targetPath, url, options).then(data => {
			this.loading.dismissAll()
			this.presentToast('Image succesfully uploaded.');
		}, err => {
			this.loading.dismissAll()
			this.presentToast('Error while uploading file.');
		});
	}


}

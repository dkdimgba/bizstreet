import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

import { DashboardPage } from '../dashboard';

/*
  Generated class for the HealthInsurance page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
	selector: 'page-cac-register',
	templateUrl: 'cac-register.html'
})
export class CacRegisterPage {
	constructor(public navCtrl: NavController, public navParams: NavParams) {}

	ionViewDidLoad() {
		console.log('ionViewDidLoad CacRegisterPage');
	}

	goToConfirm(){
		alert("We got your back. One of our representative will get in touch with you using your preferd mode of communication. And trust us, we will register this business for you in l4 working days")
		this.navCtrl.push(DashboardPage);
	}

}

import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

import {
 GoogleMap,
 GoogleMapsEvent,
 GoogleMapsLatLng,
 CameraPosition,
 GoogleMapsMarkerOptions,
 GoogleMapsMarker
} from 'ionic-native';

@Component({
  selector: 'page-search-map',
  templateUrl: 'search-map.html'
})
export class SearchMapPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {}


  ionViewDidLoad() {
    console.log('ionViewDidLoad SearchMapPage');
  }
  	// Load map only after view is initialize
	ngAfterViewInit() {
	 this.loadMap();
	}


	loadMap() {
	 // make sure to create following structure in your view.html file
	 // and add a height (for example 100%) to it, else the map won't be visible
	 // <ion-content>
	 //  <div #map id="map" style="height:100%;"></div>
	 // </ion-content>

	 // create a new map by passing HTMLElement
	 let element: HTMLElement = document.getElementById('map');

	 let map = new GoogleMap(element);

	 // create LatLng object
	 let ionic: GoogleMapsLatLng = new GoogleMapsLatLng(9.0408,7.5193);

	 // create CameraPosition
	 let position: CameraPosition = {
	   target: ionic,
	   zoom: 18,
	   tilt: 30
	 };

	 // listen to MAP_READY event
	 map.one(GoogleMapsEvent.MAP_READY).then(() => {
	   // move the map's camera to position
	   map.moveCamera(position); // works on iOS and Android
	});


	 // create new marker
	 let markerOptions: GoogleMapsMarkerOptions = {
	   position: ionic,
	   title: 'Ionic'
	 };

	 map.addMarker(markerOptions)
	   .then((marker: GoogleMapsMarker) => {
	      marker.showInfoWindow();
	    });
	 }



}

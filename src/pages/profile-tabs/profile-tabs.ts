import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

import { ProfilePage } from '../profile';
// import { galleryPage } from '../gallery';

@Component({
  selector: 'page-profile-tabs',
  templateUrl: 'profile-tabs.html'
})
export class ProfileTabsPage {

  searchString;
  tab1: any;
  // tab2: any;
  params;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  	this.tab1 = ProfilePage;
  	// this.tab2 = galleryPage;
    this.params = navParams.data;
  }

  ngOnInit(){
    this.searchString = this.navParams.get('searchString');
    console.log(this.searchString);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SearchResultPage');
  }

}

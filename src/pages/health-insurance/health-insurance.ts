import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

import { DashboardPage } from '../dashboard';

/*
  Generated class for the HealthInsurance page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
	selector: 'page-health-insurance',
	templateUrl: 'health-insurance.html'
})
export class HealthInsurancePage {
	constructor(public navCtrl: NavController, public navParams: NavParams) {}

	ionViewDidLoad() {
		console.log('ionViewDidLoad HealthInsurancePage');
	}

	goToConfirm(){
		alert("Thank you for subscribing to our Health Insurance program. One of our representative will get in touch with you using your preferd mode of communication.")
		this.navCtrl.push(DashboardPage);
	}

}

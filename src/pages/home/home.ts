import { Component, OnInit } from '@angular/core';

import { NavController, MenuController } from 'ionic-angular';
// import { Geolocation } from '@ionic-native/geolocation';


import { LoginPage } from '../login';
import { SignupPage } from '../signup';
import { SearchPage } from '../search';
import { SearchResultPage } from '../search-result';
import { BusinessDetailsPage } from '../business-details'
import { SpecialOffersPage } from '../special-offers';

import { TabsService } from '../../services/tab.service';
import { BusinessServices } from '../../services/business.service';

declare var google;

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})

export class HomePage implements OnInit {

    businesses;
    map: any;
    mapElement: any;
    search:any = '';
    
    constructor(public navCtrl: NavController, menu: MenuController, public _tabs:TabsService, private _busService:BusinessServices) { 
        this._tabs.hide();
        menu.enable(true);
    }

    ngOnInit() {
        this.getBusinesses();
        // this.getGeoLocation();
    }

    // getGeoLocation(){
    //     this.geolocation.getCurrentPosition().then((position) => {
    //         console.log(position.coords.latitude);
    //         console.log(position.coords.longitude);
    //         let latLng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
    //     })
    // }

    goToSearchList(string) {
        let search_str = string.replace(/ /g,'+');
        console.log(search_str);
        this.navCtrl.push(SearchResultPage, {
            searchString: JSON.stringify(search_str)
        });
    }

    getBusinesses() {	
		this._busService.getBusinesses()
		.subscribe(res =>{
			this.businesses = res.results;
            // this.haveData=true;
		})
	}

    goToLogin() {
        this.navCtrl.push(LoginPage);
    }
    
    goToRegister() {
        this.navCtrl.push(SignupPage);
    }

    goToSearch() {
        this.navCtrl.push(SearchPage);
    }

    goToSpecialOffers() {
        this.navCtrl.push(SpecialOffersPage);
    }
    
    goToBusinessDetails(BID) {
        this.navCtrl.push(BusinessDetailsPage, {
            bId : BID
        });
    }
}
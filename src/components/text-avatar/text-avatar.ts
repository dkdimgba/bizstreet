import { Directive } from '@angular/core';

/*
  Generated class for the TextAvatar directive.

  See https://angular.io/docs/ts/latest/api/core/index/DirectiveMetadata-class.html
  for more info on Angular 2 Directives.
*/
@Directive({
  selector: '[text-avatar]' // Attribute selector
})

export class TextAvatar {

  constructor() {
    console.log('Hello TextAvatar Directive');
  }

}

import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { Cookie } from 'ng2-cookies/ng2-cookies';

import { SignupPage } from '../signup';
import { DashboardPage } from '../dashboard';
import { HomePage } from '../home/home';

import { BusinessServices } from '../../services/business.service';


@Component({
  selector: 'page-login',
  templateUrl: 'login.html'
})
export class LoginPage {
	user:any = {};
  constructor(public navCtrl: NavController, public navParams: NavParams, private _busService:BusinessServices) {}

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  } 

    goBackHome() {
        this.navCtrl.push(HomePage);
    }

    goToSignup() {
        this.navCtrl.push(SignupPage);
    }

    login(user){
        console.log(user);
        this._busService.loginBiz(user)
        .subscribe(res => {
            console.log(res);
            if(res.count != 0) {
                Cookie.set('userData',JSON.stringify(res.results[0]));
                alert("login Successful");
                 this.navCtrl.push(DashboardPage);
            }else{
                alert("Invalid Username and Password");
                 return false;
            }            
        } );
    }
}
